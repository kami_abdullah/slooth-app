import React, { useState } from "react";
import { View, Dimensions } from "react-native";
import Splash from "./src/Components/Splash";
import Login from "./src/Components/Login";
import Createaccount from "./src/Components/Createaccount";
import Createaccountform from "./src/Components/Createaccountform";
import Createaccountname from "./src/Components/Createaccountname";
import Welcome from "./src/Components/Welcome";
import UserProfileDetails from "./src/Components/UserProfileDetails";
import BusinessProfileDetails from "./src/Components/BusinessProfileDetails";
import Routes from "./Routes"

const dim = Dimensions.get("window");

const App = () => {
  const [value, setValue] = useState(0);
  setTimeout(() => {
    const newValue = false;
    setValue(newValue);
  }, 3000);

  return (
    <View style={{ width: dim.width, height: dim.height }}>
     {value === false ? <Routes /> : <Splash />}
    </View>
    );
  };
  // <BusinessProfileDetails />

export default App;
