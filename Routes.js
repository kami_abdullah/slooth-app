import { createStackNavigator, createAppContainer } from "react-navigation";
import Login from "./src/Components/Login";
import Createaccount from "./src/Components/Createaccount";
import Createaccountform from "./src/Components/Createaccountform";
import Createaccountname from "./src/Components/Createaccountname";
import Welcome from "./src/Components/Welcome";
import UserProfileDetails from "./src/Components/UserProfileDetails";
import BusinessProfileDetails from "./src/Components/BusinessProfileDetails";

const MainNavigator = createStackNavigator(
  {
    Login: { screen: Login },
    Createaccount: { screen: Createaccount },
    Createaccountform: { screen: Createaccountform },
    Createaccountname: { screen: Createaccountname },
    Welcome: { screen: Welcome },
    UserProfileDetails: { screen: UserProfileDetails },
    BusinessProfileDetails: { screen: BusinessProfileDetails }
  },
  {
    headerMode: "none",
    navigationOptions: {
      headerVisible: false
    }
  }
);

const Routes = createAppContainer(MainNavigator);

export default Routes;
