import React from "react";
import { StyleSheet, ScrollView, View, TouchableOpacity } from "react-native";
import { Text, Input, Item, Form, Picker, Icon } from "native-base";
import Header from "./Header";
const BusinessProfileDetails = ({...props}) => {
  return (
    <View style={{ flex: 1, marginBottom: 35 }}>
      <ScrollView>
        <Header navigation={props.navigation} text={"Business Profile Details"} />

        <View>
          <View style={{ marginLeft: 10, marginRight: 10 }}>
            <Item
              rounded
              style={{
                borderWidth: 5,
                borderRadius: 5,
                margin: 5,
                backgroundColor: "#fafafa"
              }}
            >
              <Input style={{ fontSize: 14 }} placeholder="Company Name" />
            </Item>

            <Item
              rounded
              style={{
                borderWidth: 5,
                borderRadius: 5,
                margin: 5,
                backgroundColor: "#fafafa"
              }}
            >
              <Input style={{ fontSize: 14 }} placeholder="City" />
            </Item>

            <Item
              rounded
              style={{
                borderWidth: 5,
                borderRadius: 5,
                margin: 5,
                backgroundColor: "#fafafa"
              }}
            >
              <Input style={{ fontSize: 14 }} placeholder="State" />
            </Item>

            <Item
              rounded
              style={{
                borderWidth: 5,
                borderRadius: 5,
                margin: 5,
                backgroundColor: "#fafafa"
              }}
            >
              <Input style={{ fontSize: 14 }} placeholder="Zip Code" />
            </Item>

            <Item
              rounded
              style={{
                borderWidth: 5,
                borderRadius: 5,
                margin: 5,
                backgroundColor: "#fafafa"
              }}
            >
              <Input style={{ fontSize: 14 }} placeholder="Phone" />
            </Item>

            <Item
              rounded
              style={{
                borderWidth: 5,
                borderRadius: 5,
                margin: 5,
                backgroundColor: "#fafafa"
              }}
            >
              <Input style={{ fontSize: 14 }} placeholder="Contact Name" />
            </Item>

            <Form
              style={{
                marginTop: 10,
                borderWidth: 1,
                borderRadius: 5,
                borderColor: "#d6d6d6"
              }}
            >
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                headerStyle={{ backgroundColor: "#b95dd3" }}
                headerBackButtonTextStyle={{ color: "#fff" }}
                headerTitleStyle={{ color: "#fff" }}
                selectedValue={"key0"}
                onValueChange={() => {}}
              >
                <Picker.Item label="Business Category" value="key0" />
                <Picker.Item label="ATM Card" value="key1" />
                <Picker.Item label="Debit Card" value="key2" />
                <Picker.Item label="Credit Card" value="key3" />
                <Picker.Item label="Net Banking" value="key4" />
              </Picker>
            </Form>

            <Form
              style={{
                marginTop: 10,
                borderWidth: 1,
                borderRadius: 5,
                borderColor: "#d6d6d6"
              }}
            >
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                headerStyle={{ backgroundColor: "#b95dd3" }}
                headerBackButtonTextStyle={{ color: "#fff" }}
                headerTitleStyle={{ color: "#fff" }}
                selectedValue={"key0"}
                onValueChange={() => {}}
              >
                <Picker.Item label="Business Localization" value="key0" />
                <Picker.Item label="ATM Card" value="key1" />
                <Picker.Item label="Debit Card" value="key2" />
                <Picker.Item label="Credit Card" value="key3" />
                <Picker.Item label="Net Banking" value="key4" />
              </Picker>
            </Form>

            <View style={{ marginTop: 15 }}>
              <TouchableOpacity
                style={{
                    marginBottom:15,
                  backgroundColor: "white",
                  padding: 15,
                  borderRadius: 5,
                  borderWidth: 2,
                  borderColor: "#2b327c",
                  borderStyle: "dashed"
                }}
              >
                <Text style={{ textAlign: "center", color: "#2b327c" }}>
                Upload Company Logo
                </Text>
              </TouchableOpacity>
            </View>
            <View style={{ margin: 5 }} />

            <View style={{ marginTop: 10 }}>
              <TouchableOpacity
                style={{
                  backgroundColor: "#2b327c",
                  padding: 15,
                  borderRadius: 5
                }}
              >
                <Text style={{ textAlign: "center", color: "white" }}>
                  SAVE CHANGES
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  profileImgContainer: {
    marginLeft: 8,
    height: 180,
    width: 180,
    borderRadius: 90,
    alignSelf: "center",
    marginTop: 25
    // marginBottom:10
  },
  profileImg: {
    height: 180,
    width: 180,
    borderRadius: 90
  },
  edit: {
    color: "#2b327c",
    textAlign: "center",
    fontSize: 15,
    fontWeight: "bold",
    margin: 7
  }
});

export default BusinessProfileDetails;
