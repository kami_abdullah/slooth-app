import React from "react";
import {
  StyleSheet,
  ScrollView,
  View,
  Image,
  TouchableOpacity
} from "react-native";
import { Footer, Text, Input, Item } from "native-base";

const Createaccount = ({...props}) => {
  return (
    <View style={{ flex: 1 }}>
      <ScrollView>
        <View style={{ margin: 50, marginTop: 12, marginBottom: 12 }}>
          <Image
            style={{ width: 250, resizeMode: "contain" }}
            source={require("../../assets/logo.png")}
          />
        </View>
        <View>
          <View style={{ margin: 10, marginTop: 5 }}>
            <Text
              style={{ fontSize: 26, color: "#434244", textAlign: "center" }}
            >
              Create New Account
            </Text>

            <View style={{ marginTop: 15 }}>
              <Text
                style={{ fontSize: 19, textAlign: "center", color: "#555456" }}
              >
                Are you a Pet Parent
              </Text>
              <Text
                style={{ fontSize: 19, textAlign: "center", color: "#555456" }}
              >
                or Pet Related Business Owner?
              </Text>
            </View>

            <View style={{ margin: 5 }} />

            <View style={{ marginTop: 10 }}>
              <TouchableOpacity
              onPress={() => props.navigation.navigate("Createaccountform")}
                style={{
                  backgroundColor: "#2b327c",
                  padding: 15,
                  borderRadius: 5
                }}
              >
                <Text style={{ textAlign: "center", color: "white" }}>
                  PET PARENT
                </Text>
              </TouchableOpacity>
            </View>

            <View style={{ marginTop: 15 }}>
              <TouchableOpacity
              onPress={() => props.navigation.navigate("Createaccountform")}
                style={{
                  backgroundColor: "white",
                  padding: 15,
                  borderRadius: 5,
                  borderWidth: 2,
                  borderColor: "#2b327c"
                }}
              >
                <Text style={{ textAlign: "center", color: "#2b327c" }}>
                  BUSINESS OWNER
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
      <View style={{ marginTop: 10, marginBottom: 20 }}>
        <Footer style={{ backgroundColor: "white", borderColor: "#dddddd" }}>
          <View style={{ display: "flex", flexDirection: "row", padding: 15 }}>
            <Text style={{ color: "#a1a1a1", fontSize: 14 }}>
              Already have an account?
            </Text>
            <TouchableOpacity
              onPress={() => props.navigation.navigate("Login")}
            >
              <Text
                style={{
                  color: "#2b327c",
                  fontSize: 14,
                  marginLeft: 5,
                  fontWeight: "bold"
                }}
              >
                Sign In
              </Text>
            </TouchableOpacity>
          </View>
        </Footer>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({});

export default Createaccount;
