import React from "react";
import {
  StyleSheet,
  ScrollView,
  View,
  Image,
  TouchableOpacity
} from "react-native";
import { Text } from "native-base";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

const Welcome = ({...props}) => {
  return (
    <ScrollView style={{ flex: 1 }}>
      <View style={{ margin: 50, marginTop: 12, marginBottom: 12 }}>
        <Image
          style={{ width: 250, resizeMode: "contain" }}
          source={require("../../assets/logo.png")}
        />
      </View>
      <View>
        <View style={{ margin: 10, marginTop: 5 }}>
          <Text style={{ fontSize: 26, color: "#434244", textAlign: "center" }}>
            Welcome to SLOOTH!
          </Text>
          <View
            style={{
              borderWidth: 2,
              borderRadius: 75,
              width: 115,
              padding: 17,
              alignSelf: "center",
              marginTop: 35,
              marginBottom: 20
            }}
          >
            <Icon name="check" size={75} color="#2b327c" />
          </View>
          <View style={{ marginTop: 15 }}>
              <TouchableOpacity
              onPress={()=>props.navigation.navigate("UserProfileDetails")}
                style={{
                  backgroundColor: "white",
                  padding: 15,
                  borderRadius: 5,
                  borderWidth: 2,
                  borderColor: "#2b327c"
                }}
              >
                <Text style={{ textAlign: "center", color: "#2b327c" }}>
                COMPLETE YOUR PROFILE
                </Text>
              </TouchableOpacity>
            </View>

          <View style={{ marginTop: 10, marginBottom: 20 }}>
            <TouchableOpacity
              style={{
                backgroundColor: "#2b327c",
                padding: 15,
                borderRadius: 5
              }}
            >
              <Text style={{ textAlign: "center", color: "white" }}>ADD NEW PET PROFILE</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({});

export default Welcome;
