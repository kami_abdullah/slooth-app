import React from "react";
import {
  StyleSheet,
  ScrollView,
  View,
  Image,
  TouchableOpacity
} from "react-native";
import { Footer, Text, Input, Item } from "native-base";

const Createaccount = ({...props}) => {
  return (
    <View style={{ flex: 1 }}>
      <ScrollView>
        <View style={{ margin: 50, marginTop: 12, marginBottom: 12 }}>
          <Image
            style={{ width: 250, resizeMode: "contain" }}
            source={require("../../assets/logo.png")}
          />
        </View>
        <View>
          <View style={{ margin: 10, marginTop: 5 }}>
            <Text
              style={{ fontSize: 26, color: "#434244", textAlign: "center" }}
            >
              Create New Account
            </Text>

            <Item
              rounded
              style={{
                borderWidth: 5,
                borderRadius: 5,
                margin: 5,
                backgroundColor: "#fafafa"
              }}
            >
              <Input
                style={{ color: "#ababab", fontSize: 14 }}
                placeholder="Email"
              />
            </Item>

            <Item
              rounded
              style={{
                borderWidth: 5,
                borderRadius: 5,
                margin: 5,
                backgroundColor: "#fafafa"
              }}
            >
              <Input
                style={{ color: "#ababab", fontSize: 14 }}
                placeholder="Password"
              />
            </Item>

            <Item
              rounded
              style={{
                borderWidth: 5,
                borderRadius: 5,
                margin: 5,
                backgroundColor: "#fafafa"
              }}
            >
              <Input
                style={{ color: "#ababab", fontSize: 14 }}
                placeholder="Confirm Password"
              />
            </Item>

            <View style={{ margin: 5 }} />

            <View style={{ marginTop: 10 }}>
              <TouchableOpacity
                onPress={() => props.navigation.navigate("Createaccountname")}
                style={{
                  backgroundColor: "#2b327c",
                  padding: 15,
                  borderRadius: 5
                }}
              >
                <Text style={{ textAlign: "center", color: "white" }}>
                  SIGN UP
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
      <View style={{ marginTop: 10, marginBottom: 20 }}>
        <Footer style={{ backgroundColor: "white", borderColor: "#dddddd" }}>
          <View style={{ display: "flex", flexDirection: "row", padding: 15 }}>
            <Text style={{ color: "#a1a1a1", fontSize: 14 }}>
              Already have an account?
            </Text>
            <TouchableOpacity
              onPress={() => props.navigation.navigate("Login")}
            >
              <Text
                style={{
                  color: "#2b327c",
                  fontSize: 14,
                  marginLeft: 5,
                  fontWeight: "bold"
                }}
              >
                Sign In
              </Text>
            </TouchableOpacity>
          </View>
        </Footer>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({});

export default Createaccount;
