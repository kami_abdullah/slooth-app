import React from "react";
import {
  StyleSheet,
  View,
  Image,
} from "react-native";


const Splash = () => {
  return (
    <View style={{ flex: 1 }}>
      <View style={{ alignItems: "center", margin: 150 }}>
        <Image
          style={{ resizeMode: "contain" }}
          source={require("../../assets/logo.png")}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({});

export default Splash;
