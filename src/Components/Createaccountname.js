import React from "react";
import {
  StyleSheet,
  ScrollView,
  View,
  Image,
  TouchableOpacity
} from "react-native";
import { Text, Input, Item } from "native-base";
const Createaccount = ({...props}) => {
  return (
    <ScrollView style={{ flex: 1 }}>
      <View style={{ margin: 50, marginTop: 12, marginBottom: 12 }}>
        <Image
          style={{ width: 250, resizeMode: "contain" }}
          source={require("../../assets/logo.png")}
        />
      </View>
      <View>
        <View style={{ margin: 10, marginTop: 5 }}>
          <Text style={{ fontSize: 26, color: "#434244", textAlign: "center" }}>
            Enter Your Full Name
          </Text>

          <Item
            rounded
            style={{
              borderWidth: 5,
              borderRadius: 5,
              margin: 5,
              backgroundColor: "#fafafa"
            }}
          >
            <Input
              style={{ color: "#ababab", fontSize: 14 }}
              placeholder="Full Name"
            />
          </Item>
          <View>
            <Text style={{ color: "#b8b8b8", fontSize: 14 }}>
              Using your real name makes it easier for getting help from our
              community.
            </Text>
          </View>
          <TouchableOpacity
            style={{
              marginTop: 10,
              alignSelf: "center",
              backgroundColor: "#e9eaf1",
              padding: 10,
              borderRadius: 60,
              width: 120,
              height: 120
            }}
          >
            <View
              style={{
                backgroundColor: "#c2c4d9",
                padding: 10,
                borderRadius: 50,
                width: 100,
                height: 100
              }}
            >
              <View
                style={{
                  backgroundColor: "#2b327c",
                  padding: 24,
                  borderRadius: 60,
                  width: 80
                }}
              >
                <Image source={require("../../assets/camera.png")} />
              </View>
            </View>
          </TouchableOpacity>
          <View style={{ marginTop: 10 }}>
            <Text
              style={{ textAlign: "center", color: "#434244", fontSize: 14 }}
            >
              Add Profile Picture
            </Text>
          </View>

          <View style={{ margin: 5 }} />

          <View style={{ marginTop: 10, marginBottom: 20 }}>
            <TouchableOpacity
              onPress={() => props.navigation.navigate("Welcome")}
              style={{
                backgroundColor: "#2b327c",
                padding: 15,
                borderRadius: 5
              }}
            >
              <Text style={{ textAlign: "center", color: "white" }}>NEXT</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({});

export default Createaccount;
