import React from "react";
import {
  StyleSheet,
  ScrollView,
  View,
  Image,
  TouchableOpacity
} from "react-native";
import { Footer, Text, Input, Item } from "native-base";

const Login = ({ ...props }) => {
  console.log(props);
  return (
    <View style={{ flex: 1 }}>
      <ScrollView>
        <View style={{ margin: 50, marginTop: 12, marginBottom: 12 }}>
          <Image
            style={{ width: 250, resizeMode: "contain" }}
            source={require("../../assets/logo.png")}
          />
        </View>
        <View>
          <View style={{ margin: 10, marginTop: 5 }}>
            <Item
              rounded
              style={{
                borderWidth: 5,
                borderRadius: 5,
                margin: 5,
                backgroundColor: "#fafafa"
              }}
            >
              <Input
                style={{ color: "#ababab", fontSize: 14 }}
                placeholder="Email"
              />
            </Item>

            <Item
              rounded
              style={{
                borderWidth: 5,
                borderRadius: 5,
                margin: 5,
                backgroundColor: "#fafafa"
              }}
            >
              <Input
                style={{ color: "#ababab", fontSize: 14 }}
                placeholder="Password"
              />
            </Item>

            <View style={{ margin: 5 }}>
              <Text
                style={{
                  fontSize: 14,
                  textAlign: "right",
                  color: "#2a3179",
                  fontWeight: "bold"
                }}
              >
                Forgot Password?
              </Text>
            </View>
            <View style={{ marginTop: 10 }}>
              <TouchableOpacity
                style={{
                  backgroundColor: "#5fb64c",
                  padding: 15,
                  borderRadius: 5
                }}
              >
                <Text style={{ textAlign: "center", color: "white" }}>
                  LOG IN
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                marginTop: 20,
                marginBottom: 20,
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-around"
              }}
            >
              <View
                style={{
                  bottom: 10,
                  borderBottomColor: "#dddddd",
                  borderBottomWidth: 1,
                  width: 155
                }}
              />
              <View>
                <Text
                  style={{
                    textAlign: "center",
                    width: 30,
                    color: "#dddddd",
                    alignSelf: "center"
                  }}
                >
                  OR
                </Text>
              </View>
              <View
                style={{
                  bottom: 10,
                  width: 155,
                  borderBottomColor: "#dddddd",
                  borderBottomWidth: 1
                }}
              />
            </View>
            <View style={{ marginTop: 5 }}>
              <TouchableOpacity
                style={{
                  backgroundColor: "#3b5899",
                  padding: 15,
                  borderRadius: 5
                }}
              >
                <Text style={{ textAlign: "center", color: "white" }}>
                  LOG IN WITH FACEBOOK
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={{ marginTop: 10, marginBottom: 20 }}>
          <Footer style={{ backgroundColor: "white", borderColor: "#dddddd" }}>
            <View
              style={{ display: "flex", flexDirection: "row", padding: 15 }}
            >
              <Text style={{ color: "#a1a1a1", fontSize: 14 }}>
                Don’t have a SLOOTH account yet?
              </Text>
              <TouchableOpacity
                onPress={() => props.navigation.navigate("Createaccount")}
              >
                <Text
                  style={{
                    color: "#2b327c",
                    fontSize: 14,
                    marginLeft: 5,
                    fontWeight: "bold"
                  }}
                >
                  Sign Up
                </Text>
              </TouchableOpacity>
            </View>
          </Footer>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({});

export default Login;
