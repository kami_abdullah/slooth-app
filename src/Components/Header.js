import React, { Component } from "react";
import { Dimensions } from "react-native";
import { Header, Left, Body, Right, Button, Title, View } from "native-base";
import Icon from "react-native-vector-icons/Ionicons";

class HeaderBar extends Component {
  constructor(props) {
    super(props);
    console.log(props);
  }
  render() {
    return (
      <Header
        style={{ backgroundColor: "white", paddingTop: 55, paddingBottom: 30 }}
      >
        <Left>
          <Button
            transparent
            onPress={() => {
              // console.log
              this.props.navigation.pop();
            }}
          >
            <Icon name="ios-arrow-round-back" size={40} color="#2c327d" />
          </Button>
        </Left>
        <Body>
          <Title style={{ color: "#2c327d", fontWeight: "bold" }}>
            {this.props.text}
          </Title>
        </Body>
      </Header>
    );
  }
}
export default HeaderBar;
