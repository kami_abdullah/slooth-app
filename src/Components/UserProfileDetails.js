import React from "react";
import {
  StyleSheet,
  ScrollView,
  View,
  TouchableOpacity,
  Image,
  TouchableHighlight
} from "react-native";
import { Footer, Text, Input, Item } from "native-base";
import Header from "./Header";

const UserProfileDetails = ({...props}) => {

  return (
    <View style={{ flex: 1,marginBottom: 35 }}>
      <ScrollView>
        <Header navigation={props.navigation} text={"User Profile Details"} />
        <TouchableHighlight style={[styles.profileImgContainer]}>
          <Image
            source={require("../../assets/profile.jpg")}
            style={styles.profileImg}
          />
        </TouchableHighlight>
        <Text style={styles.edit}>Edit</Text>

        <View>
          <View style={{ marginLeft: 10, marginRight: 10 }}>
            <Item
              rounded
              style={{
                borderWidth: 5,
                borderRadius: 5,
                margin: 5,
                backgroundColor: "#fafafa"
              }}
            >
              <Input
                style={{ fontSize: 14 }}
                placeholder="First Name"
              />
            </Item>

            <Item
              rounded
              style={{
                borderWidth: 5,
                borderRadius: 5,
                margin: 5,
                backgroundColor: "#fafafa"
              }}
            >
              <Input
                style={{ fontSize: 14 }}
                placeholder="Last Name"
              />
            </Item>

            <Item
              rounded
              style={{
                borderWidth: 5,
                borderRadius: 5,
                margin: 5,
                backgroundColor: "#fafafa"
              }}
            >
              <Input
                style={{ fontSize: 14 }}
                placeholder="Email"
                defaultValue="evan.daniel@gmail.com"
              />
            </Item>

            <Item
              rounded
              style={{
                borderWidth: 5,
                borderRadius: 5,
                margin: 5,
                backgroundColor: "#fafafa"
              }}
            >
              <Input
                style={{ fontSize: 14 }}
                placeholder="Password"
              />
            </Item>

            <View style={{ margin: 5 }}>
              <Text
                style={{
                  fontSize: 14,
                  textAlign: "right",
                  color: "#2a3179",
                  fontWeight: "bold"
                }}
              >
                Forgot Password?
              </Text>
            </View>

            <Item
              rounded
              style={{
                borderWidth: 5,
                borderRadius: 5,
                margin: 5,
                backgroundColor: "#fafafa"
              }}
            >
              <Input
                style={{ fontSize: 14 }}
                placeholder="Phone (Optional)"
              />
            </Item>

            <View style={{marginTop:10,marginBottom:10}}>
            <Text style={{ color: "#b8b8b8", fontSize: 14 }}>
            Alows Pet Parent to receive SLOOTH Alerts through their phone’s Notification Tray
            </Text>
          </View>

            <Item
              rounded
              style={{
                borderWidth: 5,
                borderRadius: 5,
                margin: 5,
                backgroundColor: "#fafafa"
              }}
            >
              <Input
                style={{ fontSize: 14 }}
                placeholder="Zip Code"
              />
            </Item>

            <View style={{ margin: 5 }} />

            <View style={{ marginTop: 10 }}>
              <TouchableOpacity
              onPress={()=>props.navigation.navigate("BusinessProfileDetails")}
                style={{
                  backgroundColor: "#2b327c",
                  padding: 15,
                  borderRadius: 5
                }}
              >
                <Text style={{ textAlign: "center", color: "white" }}>
                SAVE CHANGES
                </Text>
              </TouchableOpacity>
            </View>

           
          </View>
        </View>
      </ScrollView>
      
    </View>
  );
};

const styles = StyleSheet.create({
  profileImgContainer: {
    marginLeft: 8,
    height: 180,
    width: 180,
    borderRadius: 90,
    alignSelf: "center",
    marginTop: 25
    // marginBottom:10
  },
  profileImg: {
    height: 180,
    width: 180,
    borderRadius: 90
  },
  edit: {
    color: "#2b327c",
    textAlign: "center",
    fontSize: 15,
    fontWeight: "bold",
    margin: 7
  }
});

export default UserProfileDetails;
